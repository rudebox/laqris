<!doctype html>

<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="header" id="header">
  <div class="wrap hpad flex flex--center flex--justify">

    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="<?php bloginfo('name'); ?>">
    </a>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>

    <nav class="nav" role="navigation">
      <div class="nav--mobile">
        <?php scratch_main_nav(); ?>
      </div>
    </nav>

  </div>
</header>
