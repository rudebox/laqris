<?php 
/**
* Description: Lionlab variants field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');
$bg = get_sub_field('bg');
?>

<section class="variants padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>">
	<div class="variants--wrapper">
		<div class="wrap hpad variants__container">
			<div class="row flex flex--wrap flex--center">
				<?php  
					$title = get_sub_field('title');
					$img = get_sub_field('img');
					$text = get_sub_field('text');
					$link = get_sub_field('link');
				?>

				<div class="col-sm-6 variants__product-img wow fadeInLeft">
					<?php if ($img) : ?>
					<img src="<?php echo esc_url($img['url']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
					<?php endif; ?>
				</div>	
			
				<div class="col-sm-6 variants__item">
				
					<h3 class="variants__title"><?php echo esc_html($title); ?></h3>
					<p><?php echo esc_html($text); ?></p>

					<div class="variants__wrap flex flex--center">
						<?php if ($link) : ?>
						<a class="btn btn--orange variants__btn" target="_blank" href="<?php echo esc_url($link); ?>"><span>Køb</span></a> 
						<?php endif; ?>
						<div class="fb-share-button variants__fb" data-href="<?php echo the_permalink(); ?>" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo the_permalink(); ?>" class="fb-xfbml-parse-ignore">Del</a></div>
					</div>
				</div>
				
			</div>
		</div>
	</div>

</section>