<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/page', 'header');?> 
  <?php get_template_part('parts/content', 'layouts'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
