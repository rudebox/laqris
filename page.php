<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <p class="center">This is the page.php file</p>

  <section class="page">

    <div class="wrap hpad">

      <article id="post-<?php the_ID(); ?>"
               <?php post_class(); ?>>

        <header>
          <h1><?php the_title(); ?></h1>
        </header>

        <?php the_content(); ?>

      </article>

    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>